#!/bin/bash

function fib {
    n=$1
    a=$2
    b=$3

    if ((n == 0)); then
        return
    fi

    temp=$a
    a=$b
    b=$((temp + a))
    echo -n "$b "

    fib $((n - 1)) $a $b
}

while true; 
do
    echo -n "Enter your Number: "
    read number

    if ((number > 3))
    then
        break
    else
        echo "Number not greater than 3."
    fi
done

a=0
b=1
echo -n "$a $b "

fib $((number - 2)) $a $b

echo

